package com.example.nishantghanate.fingerprintauth;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

@RequiresApi(api = Build.VERSION_CODES.M)
class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;
    public FingerprintHandler(Context mainActivity) {
        this.context = mainActivity;
    }




    public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(context, "Auth Failed", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        Toast.makeText(context, "Auth help", Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        Toast.makeText(context, "Auth Success", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(context, HomeActivity.class);
        String message = "Success " ;
        intent.putExtra(EXTRA_MESSAGE, message);
        context.startActivity(intent);
        ((MainActivity)(context)).finish();


    }
}
